import json

with open('output.json', 'r', encoding='utf8') as json_file:
    json_string = json_file.read()
    dex_backup = json.loads(json_string)

titles = [manga['title'] for manga in dex_backup['manga']]
print(len(titles))
