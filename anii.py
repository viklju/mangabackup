import requests

query = '''
query ($id: Int, $search: String) {
    Media (id: $id, search: $search) {
        id
        title {
            english
            romaji
            native
        }
    }
}
'''

variables = {
    'search': 'Fate/Zero'
}

url = 'https://graphql.anilist.co'

response = requests.post(url, json={'query': query, 'variables': variables})
print(response.content)
