# A Node.js Backup Script
The 'dex.js' logs in to Mangadex.org and prints out the users entire list of manga.
This script is still in the process of being updated to the new API.

## Installation
Install node.js then make sure you have the required node packages:
```bash
npm install mangadex-full-api@4.4.0
```

# Python scripts
This repo also contain two Python scripts.
The 'read-dex-backup.py' enumerates the manga output from the 'dex.js' script.
And the 'anii.py' is a proof-of-concept for communicating with the Aniilist API.
