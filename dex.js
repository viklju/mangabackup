// written in node.js run with: node dex.js > output.json

const api = require('mangadex-full-api');                 // npm install mangadex-full-api@4.4.0
// const fs = require('fs');                              // npm install fs@0.0.1-security

// TODO: filter on READING/COMPLETED https://github.com/md-y/mangadex-full-api/blob/master/src/enum/viewing-categories.js
// API documentation: https://github.com/md-y/mangadex-full-api
// TODO: write to json https://futurestud.io/tutorials/node-js-human-readable-json-stringify-with-spaces-and-line-breaks


const username = ""
const password = ""

api.agent.login(username, password, false).then(() => { // login as user, don't cache credentials
  api.agent.fillUser().then(() => {                     // gather user information
    // console.log(api.agent.user);                     // print user information
    let id = api.agent.user.list.id;

    api.agent.user.list.fill(id).then(() => {           // gather list information
      console.log(api.agent.user.list.manga);           // print manga IDs in list

      api.agent.user.list.manga.fill(id).then(() => {   // gather manga information WIP!
          console.log(api.agent.user.list.manga.title); // print manga titles WIP!
      });

    });

  });
});
